import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

public class Matrix {
	private int M; // number of rows
	private int N; // number of columns
	private double[][] data; // M-by-N array

	// create M-by-N matrix of 0's
	public Matrix(int M, int N) {
		this.M = M;
		this.N = N;
		data = new double[M][N];
	}

	public Matrix(ArrayList<ArrayList<Double>> matrix) {
		int M = matrix.size();
		int N = matrix.get(0).size();
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				this.data[i][j] = matrix.get(i).get(j);
	}

	public double getItem(int r, int c) {
		return this.data[r][c];
	}

	public void setItem(double v, int r, int c) {
		this.data[r][c] = v;
	}
	
	public double[][] getData(){
		return this.data.clone();
	}

	public static Matrix repmat(double m, int nrows, int ncols) {
		Matrix mat = new Matrix(nrows, ncols);
		for (int i = 0; i < nrows; i++) {
			for (int j = 0; j < ncols; j++) {
				mat.data[i][j] = m;
			}
		}
		return mat;
	}

	public static Matrix repmat1D(ArrayList<Double> m, int nrows, int ncols) {
		Matrix mat = new Matrix(nrows, ncols * m.size());
		for (int i = 0; i < nrows; i++) {
			for (int j = 0; j < ncols; j++) {
				for (int k = 0; k < m.size(); k++) {
					int insertInRows = i;
					int insertInCols = k + j * m.size();
					mat.data[insertInRows][insertInCols] = m.get(k);

				}
			}
		}
		return mat;
	}
	
	public static Matrix repmat1DCols(double[][] m, int nrows, int ncols) {
		Matrix mat = new Matrix(m.length, ncols);
		for (int i = 0; i < nrows; i++) {
			for (int j = 0; j < ncols; j++) {
				for (int k = 0; k < m.length; k++) {
					mat.data[k][j] = m[k][0];

				}
			}
		}
		return mat;
	}

	

	public static Matrix repmat(ArrayList<ArrayList<Double>> m, int nrows, int ncols) {
		Matrix mat = new Matrix(nrows * m.size(), ncols * m.get(0).size());
		for (int i = 0; i < nrows; i++) {
			for (int j = 0; j < ncols; j++) {
				for (int k = 0; k < m.size(); k++) {
					for (int c = 0; c < m.get(0).size(); c++) {
						int insertInRows = k + i * m.size();
						int insertInCols = c + j * m.get(0).size();
						mat.data[insertInRows][insertInCols] = m.get(k).get(c);
					}
				}
			}
		}
		return mat;
	}

	// create matrix based on 2d array
	public Matrix(double[][] data) {
		M = data.length;
		N = data[0].length;
		this.data = new double[M][N];
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				this.data[i][j] = data[i][j];
	}

	// copy constructor
	private Matrix(Matrix A) {
		this(A.data);
	}

	// create and return a random M-by-N matrix with values between 0 and 1
	public static Matrix random(int M, int N) {
		Matrix A = new Matrix(M, N);
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				A.data[i][j] = Math.random();
		return A;
	}

	// create and return the N-by-N identity matrix
	public static Matrix identity(int N) {
		Matrix I = new Matrix(N, N);
		for (int i = 0; i < N; i++)
			I.data[i][i] = 1;
		return I;
	}

	// swap rows i and j
	private void swap(int i, int j) {
		double[] temp = data[i];
		data[i] = data[j];
		data[j] = temp;
	}

	// create and return the transpose of the invoking matrix
	public Matrix transpose() {
		Matrix A = new Matrix(N, M);
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				A.data[j][i] = this.data[i][j];
		return A;
	}

	// return C = A + B
	public Matrix plus(Matrix B) {
		Matrix A = this;
		if (B.M != A.M || B.N != A.N)
			throw new RuntimeException("Illegal matrix dimensions.");
		Matrix C = new Matrix(M, N);
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				C.data[i][j] = A.data[i][j] + B.data[i][j];
		return C;
	}

	// return C = A - B
	public Matrix minus(Matrix B) {
		Matrix A = this;
		if (B.M != A.M || B.N != A.N)
			throw new RuntimeException("Illegal matrix dimensions.");
		Matrix C = new Matrix(M, N);
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				C.data[i][j] = A.data[i][j] - B.data[i][j];
		return C;
	}

	// does A = B exactly?
	public boolean eq(Matrix B) {
		Matrix A = this;
		if (B.M != A.M || B.N != A.N)
			throw new RuntimeException("Illegal matrix dimensions.");
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				if (A.data[i][j] != B.data[i][j])
					return false;
		return true;
	}

	// return C = A * B
	public Matrix times(Matrix B) {
		Matrix A = this;
		if (A.N != B.M)
			throw new RuntimeException("Illegal matrix dimensions.");
		Matrix C = new Matrix(A.M, B.N);
		for (int i = 0; i < C.M; i++)
			for (int j = 0; j < C.N; j++)
				for (int k = 0; k < A.N; k++)
					C.data[i][j] += (A.data[i][k] * B.data[k][j]);
		return C;
	}
	public void times(double val){
		for(int i=0;i<M;i++){
			for(int j=0;j<N;j++){
				data[i][j] *= val;
			}
		}
	}
	
	public void exp(){
		for(int i=0;i<M;i++){
			for(int j=0;j<N;j++){
				data[i][j] = Math.exp(data[i][j]);
			}
		}
	}
	
	public void add(double val){
		for(int i=0;i<M;i++){
			for(int j=0;j<N;j++){
				data[i][j] += val;
			}
		}
	}
	
	public Matrix divideMatrixUnder(double val){
		Matrix ret = new Matrix(this);
		for(int i=0;i<M;i++){
			for(int j=0;j<N;j++){
				ret.data[i][j] = 1.0/ret.data[i][j];
			}
		}
		return ret;
	}

	// return x = A^-1 b, assuming A is square and has full rank
	public Matrix solve(Matrix rhs) {
		if (M != N || rhs.M != N || rhs.N != 1)
			throw new RuntimeException("Illegal matrix dimensions.");

		// create copies of the data
		Matrix A = new Matrix(this);
		Matrix b = new Matrix(rhs);

		// Gaussian elimination with partial pivoting
		for (int i = 0; i < N; i++) {

			// find pivot row and swap
			int max = i;
			for (int j = i + 1; j < N; j++)
				if (Math.abs(A.data[j][i]) > Math.abs(A.data[max][i]))
					max = j;
			A.swap(i, max);
			b.swap(i, max);

			// singular
			if (A.data[i][i] == 0.0)
				throw new RuntimeException("Matrix is singular.");

			// pivot within b
			for (int j = i + 1; j < N; j++)
				b.data[j][0] -= b.data[i][0] * A.data[j][i] / A.data[i][i];

			// pivot within A
			for (int j = i + 1; j < N; j++) {
				double m = A.data[j][i] / A.data[i][i];
				for (int k = i + 1; k < N; k++) {
					A.data[j][k] -= A.data[i][k] * m;
				}
				A.data[j][i] = 0.0;
			}
		}

		// back substitution
		Matrix x = new Matrix(N, 1);
		for (int j = N - 1; j >= 0; j--) {
			double t = 0.0;
			for (int k = j + 1; k < N; k++)
				t += A.data[j][k] * x.data[k][0];
			x.data[j][0] = (b.data[j][0] - t) / A.data[j][j];
		}
		return x;

	}

	// print matrix to standard output
	public void show() {
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < N; j++)
				System.out.printf("%9.4f ", data[i][j]);
			System.out.println();
		}
	}
	
	public Boolean writeToFile(String fileName){
		try {
			File f = new File(fileName);
			if(!f.exists())
				f.createNewFile();
			PrintStream ps = new PrintStream(f);
			for(int i=0;i<M;i++){
				for(int j=0;j<N;j++){
					ps.print(data[i][j]);
					if(j+1<N)
						ps.print(" , ");
				}
				ps.println();
				
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public static Matrix concatRows(Matrix m1, Matrix m2){
		Matrix ret = new Matrix(m1.data.length + m2.data.length, m1.data[0].length);
		for(int i=0;i<m1.data.length;i++){
			for(int j=0;j<m1.data[0].length;j++){
				ret.data[i][j] = m1.data[i][j];
			}
		}
		for(int i=m1.data.length;i<m1.data.length + m2.data.length;i++){
			for(int j=0;j<m2.data[0].length;j++){
				ret.data[i][j] = m2.data[i-m1.data.length][j];
			}
		}
		return ret;
		
	}

	public static double[][] reshape(double[][] A, int m, int n)
			{
		int origM = A.length;
		int origN = A[0].length;
		/*if (origM * origN != m * n) {
			throw new Exception("New matrix must be of same area as matix A");
		}*/
		double[][] B = new double[m][n];
		double[] A1D = to1DColumnsRows(A);
		int index = 0;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				B[i][j] = A1D[index++];

			}

		}

		return B;

	}

	/**
	 * 
	 * Transforms 2D matrix to 1D by concatenation
	 * 
	 * @param A
	 * 
	 * @return
	 */

	public static double[] to1D(double[][] A) {

		double[] B = new double[A.length * A[0].length];

		int index = 0;

		for (int i = 0; i < A.length; i++) {

			for (int j = 0; j < A[0].length; j++) {

				B[index++] = A[i][j];

			}

		}

		return B;

	}
	public static double[] to1DColumnsRows(double[][] A) {

		double[] B = new double[A.length * A[0].length];

		int index = 0;

		for (int i = 0; i < A[0].length; i++) {

			for (int j = 0; j < A.length; j++) {

				B[index++] = A[j][i];

			}

		}

		return B;

	}


}
