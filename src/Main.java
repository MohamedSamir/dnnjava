import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import com.jmatio.io.*;
import com.jmatio.types.*;

public class Main {
	public static void main(String[] args) {
		try {
			Main.startProcessing();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public static Matrix prepareDataForTraining(double[][] newCurrentTrainingData2DList, final ArrayList<Double> frList, double nfr, double frSize ){
		int mbsz = frList.size();
		int nbat = frList.size() / mbsz;
		int numCases = mbsz*nbat;
		double tiny = 0.000000001;
		double bat = 1;
		ArrayList<Double> uttnum = new ArrayList<Double>();
		ArrayList<Double> frnum = new ArrayList<Double>();
		Matrix batIn = new Matrix((int)(nfr*frSize), mbsz);
		ArrayList<Double> batTarg = new ArrayList<Double>();
		for(int j=0;j<mbsz;j++){
			double [][] beforeReshape = new double[(int)nfr][(int)frSize];
			for(int k=0;k<nfr;k++){
				for(int c=0;c<frSize;c++){
					beforeReshape[k][c] = newCurrentTrainingData2DList[j+k][c];
				}
			}
			Matrix beforReshapeMatrix = new Matrix(beforeReshape);
			beforReshapeMatrix = beforReshapeMatrix.transpose();
			
			//beforReshapeMatrix.show();
			//System.out.println("--------------------------");
			double [][]afterReshape = Matrix.reshape(beforReshapeMatrix.getData(), (int) (nfr*frSize), 1);
			
			/*for(int q=0;q<1;q++){
				for(int w=0;w<nfr*frSize;w++){
					System.out.println(afterReshape[w][q]);
				}
			}*/
			for(int k=0;k<nfr*frSize;k++){
				batIn.setItem(afterReshape[k][0], k, j);
			}
			//batIn.show();
			//break;
		}
		return batIn;
	}
	
	public static void startProcessing() throws FileNotFoundException, IOException {
		double nfr = 11;
		double frSize = 10;
		double[] layerSizes = { 8192, 4096 };
		double gInitHidBias = -1.0;
		double initHidBias = 0.0;
		double initWeightSigma = 0.01;
		double[] ftuneParamDict = { 20, 0.0002, 0.1, 0.0001, 0.9 };// mbsz,wcost,stlrate,minrate,momentum
		String[] wVarNames = { "visToHid", "visBias", "hidBias" };
		ArrayList<ArrayList<Double>> layerArgs = new ArrayList<ArrayList<Double>>();
		ArrayList<Double> firstItemInArrayArgs = new ArrayList<Double>();
		firstItemInArrayArgs.add(frSize * nfr);
		firstItemInArrayArgs.add(layerSizes[0]);
		layerArgs.add(firstItemInArrayArgs);
		for (int i = 1; i < layerSizes.length; i++) {
			ArrayList<Double> currentItemInArrayArgs = new ArrayList<Double>();
			currentItemInArrayArgs.add(layerArgs.get(i - 1).get(1));
			currentItemInArrayArgs.add(layerSizes[i]);
		}
		ArrayList<Double> lastItemInArrayArgs = new ArrayList<Double>();
		lastItemInArrayArgs.add(layerSizes[layerSizes.length - 1]);
		lastItemInArrayArgs.add(422.0);
		layerArgs.add(lastItemInArrayArgs);


		MatFileReader layWeightsReader,trainingDataReader;
		//layWeightsReader = new MatFileReader("layWeights.mat");
		trainingDataReader = new MatFileReader("matlab.mat");
		MLCell trainingData = (MLCell) trainingDataReader.getMLArray("myData");
		//MLCell trainingWeights = (MLCell) layWeightsReader.getMLArray("layWeights");
		//System.out.println(trainingData.getSize());
		for(int i=0;i<trainingData.getSize();i++){
			MLDouble currentTrainingData = (MLDouble) trainingData.get(i);
			ArrayList<Double> frList = new ArrayList<Double>();
			int uttsz = currentTrainingData.getM();
			frList = createList(currentTrainingData.getM());
			ArrayList<Double> uttList = new ArrayList<Double>();
			double[] list = Matrix.to1D(Matrix.repmat((double)i, 1, uttsz).getData());
			for(double val : list) uttList.add(val);
			double[][] currentTrainingData2DList = currentTrainingData.getArray();
			Matrix m1 = Matrix.repmat1D(getRow(currentTrainingData2DList, 0), (int)Math.floor(nfr/2.0), 1);
			Matrix m2 = new Matrix(currentTrainingData2DList);
			Matrix m3 = Matrix.repmat1D(getRow(currentTrainingData2DList, currentTrainingData2DList.length-1), (int)Math.floor(nfr/2.0), 1);
			Matrix m4 = Matrix.concatRows(m1, m2);
			double[][] newCurrentTrainingData2DList = Matrix.concatRows(m4, m3).getData();
			Matrix batIn = prepareDataForTraining(newCurrentTrainingData2DList, frList, nfr, frSize);
			/*int mbsz = frList.size();
			int nbat = frList.size() / mbsz;
			int numCases = mbsz*nbat;
			double tiny = 0.000000001;
			double bat = 1;
			ArrayList<Double> uttnum = new ArrayList<Double>();
			ArrayList<Double> frnum = new ArrayList<Double>();
			Matrix batIn = new Matrix((int)(nfr*frSize), mbsz);
			ArrayList<Double> batTarg = new ArrayList<Double>();
			for(int j=0;j<mbsz;j++){
				double [][] beforeReshape = new double[(int)nfr][(int)frSize];
				for(int k=0;k<nfr;k++){
					for(int c=0;c<frSize;c++){
						beforeReshape[k][c] = newCurrentTrainingData2DList[j+k][c];
					}
				}
				Matrix beforReshapeMatrix = new Matrix(beforeReshape);
				beforReshapeMatrix = beforReshapeMatrix.transpose();
				
				//beforReshapeMatrix.show();
				//System.out.println("--------------------------");
				double [][]afterReshape = Matrix.reshape(beforReshapeMatrix.getData(), (int) (nfr*frSize), 1);
				
				//for(int q=0;q<1;q++){
				//	for(int w=0;w<nfr*frSize;w++){
				//		System.out.println(afterReshape[w][q]);
				//	}
				//}
				for(int k=0;k<nfr*frSize;k++){
					batIn.setItem(afterReshape[k][0], k, j);
				}
				//batIn.show();
				//break;
			}*/
			layWeightsReader = new MatFileReader("layWeights.mat");
			MLCell trainingWeights = (MLCell) layWeightsReader.getMLArray("layWeights");
			//batIn.show();
			Matrix output = makeBatch(batIn, trainingWeights);
			output.writeToFile("output"+i+".txt");
			break;
		}
		
		
		/*
		 * nfr = 11; frSize=10; layerSizes = [8192,4096];
		 * 
		 * ginitHidBias = -1.0; % for the gaussian layer initHidBias = 0.0;
		 * initWeightSigma = 0.01;
		 * 
		 * %fine tuning parameters ftuneParamDict =
		 * struct('mbsz',20,'wcost',0.0002,
		 * 'stlrate',0.1,'minrate',0.0001,'momentum',0.9); wVarNames =
		 * {'visToHid', 'visBias' , 'hidBias'}; layerArgs =
		 * cell(1,length(layerSizes)+1); layerArgs{1}=
		 * [frSize*nfr,layerSizes(1)];
		 * 
		 * for l=2:length(layerSizes) layerArgs{l} =
		 * [layerArgs{l-1}(2),layerSizes(l)]; end;
		 * layerArgs{length(layerSizes)+1} = [layerSizes(end), 422]; %the
		 * softmax output layer nlay = length(layerArgs);
		 * 
		 * [ layWeights,dlayWeights ] = initializing_weights(
		 * nlay,layerArgs,initWeightSigma,ginitHidBias,initHidBias); wpath=
		 * 'C:\Working_Code_Master\DNN_HMM\Julius_NN_Decode\w_speechDBN_15fr_1lay_50monophones_repmat_1024_test_30hours.mat';
		 * layWeights = loadweights(layWeights,wpath,wVarNames);
		 * Path='D:\Moustafa\Hafss\Neural_Network\test_non_native_speak_Correct';
		 * layersAct = cell(1,nlay+1); %contains activations for each layer,
		 * includes the input MFCC data path_3 =
		 * 'C:\Working_Code_Master\DNN_HMM\Julius_NN_DecodeTestBeam\output_layer_TrainDataForAlgin\';
		 * %path_data =
		 * 'D:\Moustafa\Hafss\Neural_Network\5)_Decoding_our_Neural\Decode_Julius\test_HMM_SPeak\';
		 * %files = dir(path_data);
		 */
	}
	
	public static ArrayList<Double> createList(int size){
		ArrayList<Double> ret = new ArrayList<Double>();
		for(int i=0;i<size;i++){
			ret.add(i+1.0);
		}
		return ret;
	}
	
	public static ArrayList<Double> getRow(double [][]mat , int rowNumber){
		ArrayList<Double> ret = new ArrayList<Double>();
		for(int i=0;i<mat[0].length;i++){
			ret.add(mat[rowNumber][i]);
		}
		return ret;
	}
	
	public static Matrix makeBatch(Matrix batData,MLCell layWeights){
		Matrix nplinAct = null;
		int nDim = batData.getData().length;
		int nCases = batData.getData()[0].length;
		double nlay = layWeights.getSize();
		for(int i=0;i<nlay-1;i++){
			MLCell currentLayer = (MLCell) layWeights.get(i);
			MLSingle weights = (MLSingle) currentLayer.get(0);
			MLSingle currentNodes = (MLSingle) currentLayer.get(2);
			//double[][] currentWeights2DArray = weights.getArray();
			double[][]currentWeights2DArray = new double[weights.getM()][weights.getN()];
			double[][]currentNodes2DArray = new double[currentNodes.getM()][currentNodes.getN()];
			for(int k=0;k<weights.getM();k++){
				for(int c=0;c<weights.getN();c++){
					currentWeights2DArray[k][c] = weights.get(k, c);
				}
			}
			for(int k=0;k<currentNodes.getM();k++){
				for(int c=0;c<currentNodes.getN();c++){
					currentNodes2DArray[k][c] = currentNodes.get(k, c);
				}
			}
			Matrix currentWeightsMatrix = new Matrix(currentWeights2DArray);
			currentWeightsMatrix = currentWeightsMatrix.transpose();
			currentWeightsMatrix.times(-1);
			Matrix firstOperandInSub = currentWeightsMatrix.times(batData);
			Matrix secondOperandInSub = Matrix.repmat1DCols(currentNodes2DArray, 1, nCases);
			Matrix underExp = firstOperandInSub.minus(secondOperandInSub);
			//underExp.show();
			/*for(int k=0;k<50;k++){
				for(int c=0;c<39;c++)
						System.out.print(underExp.getItem(k, c) + "          ");
				System.out.println();
			}*/
			underExp.exp();
			underExp.add(1.0);
			batData = new Matrix(underExp.divideMatrixUnder(1.0).getData());
			
			
			//System.out.println("---------------------------");
			//secondOperandInSub.show();
			//break;
			
		}
		
		MLCell currentLayer = (MLCell) layWeights.get((int)nlay-1);
		MLSingle weights = (MLSingle) currentLayer.get(0);
		MLSingle currentNodes = (MLSingle) currentLayer.get(2);
		//double[][] currentWeights2DArray = weights.getArray();
		double[][]currentWeights2DArray = new double[weights.getM()][weights.getN()];
		double[][]currentNodes2DArray = new double[currentNodes.getM()][currentNodes.getN()];
		for(int k=0;k<weights.getM();k++){
			for(int c=0;c<weights.getN();c++){
				currentWeights2DArray[k][c] = weights.get(k, c);
			}
		}
		for(int k=0;k<currentNodes.getM();k++){
			for(int c=0;c<currentNodes.getN();c++){
				currentNodes2DArray[k][c] = currentNodes.get(k, c);
			}
		}
		
		Matrix currentWeightsMatrix = new Matrix(currentWeights2DArray);
		currentWeightsMatrix = currentWeightsMatrix.transpose();
		Matrix firstOperandInSub = currentWeightsMatrix.times(batData);
		Matrix secondOperandInSub = Matrix.repmat1DCols(currentNodes2DArray, 1, nCases);
		nplinAct = firstOperandInSub.plus(secondOperandInSub);
		
		
		
		
		
		System.out.println("batData size is " + nplinAct.getData().length + " , " + nplinAct.getData()[0].length);
		
		
		/*for(int k=0;k<50;k++){
			for(int c=0;c<39;c++)
				System.out.print(nplinAct.getItem(k, c) + "          ");
			System.out.println();
		}*/
		return nplinAct;
	}
	
	//public static ArrayList<Double> zeros()
	
	/*
	public static ArrayList<Double> repmat(double value, int numberOfRows, int numberOfColumns){
		
	}*/
}
